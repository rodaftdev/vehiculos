package com.entity;

public enum marca {
volsvagen(1),
chevrolet(2),
toyota(3),
yamaha(4),
mercedez(5),
nissan(6),
jeep(7),
bmw(8),
ford(9),
seat(10),
audi(11);

private int idMarca;

private marca(int idMarca) {
    this.idMarca = idMarca;
}
@Override
public String toString() {
    return "marca [idMarca=" + idMarca + "]";
}
}
