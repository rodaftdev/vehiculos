package com.entity;

public enum danoV {

    Si(0),
    No (1);

    private int idDano;

    private danoV(int idDano) {
        this.idDano = idDano;
    }

    @Override
    public String toString() {
        return "danoV [idDano=" + idDano + "]";
    }

    
}
