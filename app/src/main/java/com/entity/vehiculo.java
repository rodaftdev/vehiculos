package com.entity;

import java.io.*;
import java.util.*;

public class vehiculo {
    
   private tipoV tipo;
   private String placa;
   private combustible combustible;
   private String color;
   private int año;
   private funcionV funcionV;
   private marca marca;
   private danoV dano;

   public vehiculo(tipoV tipo, String placa, combustible combustible, String color, int año,
   funcionV funcionV, marca marca, danoV dano) {
      this.tipo = tipo;
      this.placa = placa;
      this.combustible = combustible;
      this.color = color;
      this.año = año;
      this.funcionV = funcionV;
      this.marca = marca;
      this.dano = dano;
   }

   @Override
   public String toString() {
      return "vehiculo [tipoV=" + tipo + ", placa=" + placa + ", combustible=" + combustible + ", color=" + color
            + ", año=" + año + ", funcionV=" + funcionV + ", marca=" + marca + ", dano=" + dano + "]";
   }
    public String LeerArchivo(){
        String ruta="/home/jose/Documentos/Java/vehiculos/app/src/main/resources/listavehiculos.txt";
        String auto="/home/jose/Documentos/Java/vehiculos/app/src/main/resources/auto/auto.txt";
        String camion="/home/jose/Documentos/Java/vehiculos/app/src/main/resources/camion/camion.txt";
        String moto="/home/jose/Documentos/Java/vehiculos/app/src/main/resources/moto/moto.txt";
        String camioneta="/home/jose/Documentos/Java/vehiculos/app/src/main/resources/camioneta/camioneta.txt";
        String cadena="", vehiculo;
        int contador=1;
        File archivo;
        FileWriter escribir;
        PrintWriter linea;
        FileReader leer;
        BufferedReader almacenamiento;
        archivo = new File(ruta);
        try {
            leer = new FileReader(archivo);
            almacenamiento=new BufferedReader(leer);
            cadena="";
            while(cadena!=null){
                try {
                    cadena=almacenamiento.readLine();
                    vehiculo=cadena;
                    
                    if(vehiculo.charAt(0)=='1'){
                        escribir = new FileWriter(auto,true);
                        linea = new PrintWriter(escribir);
                        linea.println(vehiculo);
                        linea.close();
                        escribir.close();
                    }else if(vehiculo.charAt(0)=='2'){
                        escribir = new FileWriter(camioneta,true);
                        linea = new PrintWriter(escribir);
                        linea.println(vehiculo);
                        linea.close();
                        escribir.close();
                    }else if(vehiculo.charAt(0)=='3'){
                        escribir = new FileWriter(moto,true);
                        linea = new PrintWriter(escribir);
                        linea.println(vehiculo);
                        linea.close();
                        escribir.close();
                    }else if(vehiculo.charAt(0)=='4'){
                        escribir = new FileWriter(camion,true);
                        linea = new PrintWriter(escribir);
                        linea.println(vehiculo);
                        linea.close();
                        escribir.close();
                    }else{
                        System.out.println("Vehiculo desconocido......");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                contador++;
            }
            
        } catch (FileNotFoundException e) {
            
            e.printStackTrace();
        }
        return cadena;
        
    }

    
    
}
