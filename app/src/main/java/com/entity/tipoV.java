package com.entity;

public enum tipoV {
Auto(1),
Camioneta(2),
Motosicleta(3),
Camion(4);

  private int idTipo;

private tipoV(int idTipo) {
    this.idTipo = idTipo;
}
@Override
public String toString() {
    return "tipoV [idTipo=" + idTipo + "]";
}
}
