package com.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Operacion {

    public int idOperacion;
    private static int contOperacion;

    public Operacion() {
        this.idOperacion = ++contOperacion;
    }

    public void consultaPlaca(String placa, String nombreArchivo) {

        File archivo = new File(nombreArchivo);

        System.out.println("Archivo: " + archivo);

        try (FileReader leeArchivo = new FileReader(archivo)) {

            BufferedReader leeRenglon = new BufferedReader(leeArchivo);

            String infoRenglon = leeRenglon.readLine();
            System.out.println("Inicio lectura de archivo para buscar");
            int noEncontrado = 0;
            while (infoRenglon != null) {
                String valorSeparadoRenglon[] = infoRenglon.split(",");
                if (valorSeparadoRenglon[1].equals(placa)) {
                    System.out.println("Datos localizados !!! " + infoRenglon);
                    noEncontrado = 0;
                    break;
                } else {
                    noEncontrado++;
                }
                infoRenglon = leeRenglon.readLine();
            }
            leeRenglon.close();
            if (noEncontrado > 0) {
                System.out.println("Datos NO localizados !!! " + placa);
            }
            System.out.println("Fin lectura de archivo para buscar");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}